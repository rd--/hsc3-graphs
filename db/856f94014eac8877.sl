let mod = LfNoise2(80);
SinOsc(
	LinLin(
		mod,
		-1,
		1,
		MouseX(200, 8000, 1, 0.2),
		MouseY(200, 8000, 1, 0.2)
	),
	0
) * 0.1
